const findObjectVal = (obj, key) => {
    let keysArray = key.split('.');
    let firstKey = keysArray.shift();

    if (obj.hasOwnProperty(firstKey)) {
        if (keysArray.length) {
            return findObjectVal(obj[firstKey], keysArray.join('.'));
        } else {
            return obj[firstKey];
        }
    }

    return undefined;
}

const filterCollection = (arr, words, isAllWordsIncludes, ...keys) => {
    if (!arr instanceof Array) {
        return false;
    }
    let wordsArr = words.split(' ');
    let filterArr = [];

    for (let i in arr) {
        let isAllIncludes = true;
        let isOneInclude = false;
        for (let j in wordsArr) {
            for (let k in keys) {
                if (typeof keys[k] === 'string') {
                    let val = keys[k].includes('.') ? findObjectVal(arr[i], keys[k]) : arr[i][keys[k]];
                    if (val && val.toLowerCase() === wordsArr[j].toLowerCase()) {
                        isOneInclude = true;
                        break;
                    } else {
                        isOneInclude = false;
                    }
                }
            }
            if (!isOneInclude) {
                isAllIncludes = false;
            }
        }
        if (isAllWordsIncludes) {
            if (isAllIncludes) {
                filterArr.push(arr[i]);
            }
        } else {
            if (isOneInclude) {
                filterArr.push(arr[i]);
            }
        }
    }

    return filterArr;
}

let test = [
    {'name': 'Toyota', 'description': 'test test', 'contentType': {'name':'string'}, 'locales': {'name': 'en_US', 'description': 'test test'}},
    {'name': 'Mazda', 'description': 'test test', 'contentType': {'name':'string'}, 'locales': {'name': 'en_US', 'description': 'test test'}},
    {'name': 'toyota', 'description': 'test test', 'contentType': {'name':'string'}, 'locales': {'name': 'en_UK', 'description': 'test test'}},
];

console.log(filterCollection(test, 'en_US Toyota', true, 'name', 'description', 'contentType.name', 'locales.name', 'locales.description'));
console.log(filterCollection(test, 'en_US Toyota', false, 'name', 'description', 'contentType.name', 'locales.name', 'locales.description'));
