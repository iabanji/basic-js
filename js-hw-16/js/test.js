
const fibonacci = (a,b,n) => {
    let c = a + b;
    if (n === 0) {
        return a;
    }
    if (n === 1 || n === -1) {
        return b;
    }
    if (n === 2 || n === -2) {
        return c;
    }
    if (n > 0) {
        for (let i = 3; i <= n; i++) {
            a = b;
            b = c;
            c = a + b;
        }
    } else {
        for (let i = -3; i >= n; i--) {
            a = b;
            b = c;
            c = a + b;
        }
    }
    return c;
}

let num = undefined;

while (isNaN(num)) {
    num = +prompt('Input number:');
}

num = Number(num);

console.log(fibonacci(0,1, num));
