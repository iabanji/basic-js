
const factorial = (num) => {
    let factorial = 1;
    for (let i = 2; i <= num; i++) {
        factorial *= i;
    }
    return factorial;
}

let num = undefined;

while (isNaN(num)) {
    num = +prompt('Input number:');
}

num = Number(num);

if (num > 0) {
    console.log(factorial(num));
}