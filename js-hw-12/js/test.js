function removeBlueClass(el) {
    document.querySelectorAll('.btn.blue').forEach((element) => {
        if (element.innerText !== el.innerText) {
            element.classList.remove('blue');
        }
    });
}

document.addEventListener('keypress', (e) => {
    const keypress = e.key;
    document.querySelectorAll('.btn').forEach((el) => {
        if (el.innerText === keypress || el.innerText.toLowerCase()) {
            removeBlueClass(el);
            el.classList.add('blue');
        }
    });

});


