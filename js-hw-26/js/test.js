const images = [
    '../img/header-bg.svg',
    '../img/Logo-main.svg',
    '../img/option-icon.svg',
    '../img/option-icon2.svg',
    '../img/product1.png',
    '../img/photo_2023-04-15_14-07-54.jpg',
];

let createImage = (id) => {
    let img = document.createElement('img');
    img.setAttribute('src', images[id]);
    img.setAttribute('alt', 'Image');
    img.setAttribute('data-img-id', id);
    return img;
}

let prevButton = document.createElement('button');
prevButton.setAttribute('id', 'previous');
prevButton.innerText = 'Previous';

document.body.appendChild(prevButton);

document.body.appendChild(createImage(0));

let nextButton = document.createElement('button');
nextButton.setAttribute('id', 'next');
nextButton.innerText = 'Next';

document.body.appendChild(nextButton);

nextButton.addEventListener('click', function (e) {
    let img = document.querySelector('img');
    let counter = Number(img.getAttribute('data-img-id'));

    if (counter === images.length - 1) {
        counter = 0;
    } else {
        counter++;
    }
    img.remove();
    this.before(createImage(counter));
});

prevButton.addEventListener('click', function (e) {
    let img = document.querySelector('img');
    let counter = Number(img.getAttribute('data-img-id'));

    if (counter === 0) {
        counter = images.length - 1;
    } else {
        counter--;
    }
    img.remove();
    this.after(createImage(counter));
});
