let drawCircleButton = document.getElementById('draw-circles');

document.body.addEventListener('click', function (e) {
    if (e.target.matches('.canvas')) {
        e.target.remove();
    }
});

drawCircleButton.addEventListener('click', function (e) {
    let sizeButton = document.getElementById('size-button');
    if (!sizeButton) {
        let sizeInput = document.createElement('input');
        sizeInput.setAttribute('type', 'number');
        sizeInput.setAttribute('min', '1');
        sizeInput.setAttribute('max', '100');
        sizeInput.setAttribute('placeholder', 'діаметр кола, px');
        sizeButton = document.createElement('button');
        sizeButton.setAttribute('id', 'size-button');
        sizeButton.innerText = "Намалювати";
        this.after(sizeButton);
        this.after(sizeInput);
        this.after(document.createElement('br'));

        const br = document.createElement('br');
        sizeButton.after(br);

        sizeButton.addEventListener('click', function (e) {
            if (!sizeInput.value) {
                alert('Size input is empty');
            } else {
                let section = document.getElementById('main-section');
                if (section) {
                    section.remove();
                }
                section = document.createElement('section');
                section.setAttribute('id', 'main-section');
                br.after(section);
                const radius = Number(sizeInput.value) / 2;
                for(let i = 0; i < 10; i++) {
                    for(let j = 0; j < 10; j++) {
                        let canvas = document.createElement('canvas');
                        canvas.setAttribute('class', 'canvas');
                        canvas.style.height = sizeInput.value + 'px';
                        canvas.style.width = sizeInput.value + 'px';

                        const context = canvas.getContext('2d');
                        const centerX = canvas.width / 2;
                        const centerY = canvas.height / 2;
                        context.beginPath();
                        context.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
                        context.fillStyle = '#' + Math.floor(Math.random()*16777215).toString(16);
                        context.fill();

                        section.appendChild(canvas);
                    }
                    section.appendChild(document.createElement('br'));
                }
            }
        });
    }
});