let isDefaultTheme = true;
if (localStorage.getItem('isDefaultTheme')) {
    isDefaultTheme = localStorage.getItem('isDefaultTheme') === 'true';
}

const defaultColors = [
    {'class': '.section3-container1', 'color': '#634F2C'},
    {'class': '.section3-container2', 'color': '#e8d2abc2'},
    {'class': '.section3-container3', 'color': '#E4B564'},
    {'class': '.section3-container4', 'color': '#635A4A'},
    {'class': '.section3-container5', 'color': '#B08C4D'},
    {'class': '.section3-container6', 'color': '#634F2C'},
];

const notDefaultColors = [
    {'class': '.section3-container2', 'color': '#634F2C'},
    {'class': '.section3-container1', 'color': '#e8d2abc2'},
    {'class': '.section3-container4', 'color': '#E4B564'},
    {'class': '.section3-container3', 'color': '#635A4A'},
    {'class': '.section3-container6', 'color': '#B08C4D'},
    {'class': '.section3-container5', 'color': '#634F2C'},
];

function changeColors(isDef) {
    let colors = defaultColors;
    if (!isDef) {
        colors = notDefaultColors;
    }

    for(let i in colors) {
        document.querySelector(colors[i].class).style.backgroundColor = colors[i].color;
    }
}

if (!isDefaultTheme) {
    changeColors(isDefaultTheme);
}

window.onbeforeunload = function () {
    localStorage.setItem('isDefaultTheme', isDefaultTheme);
}

document.getElementById('btn').addEventListener('click', (e) => {
    isDefaultTheme = !isDefaultTheme;
    changeColors(isDefaultTheme);
});


