const createNewUser = () => {
    let name = null;
    while (!name || !isNaN(name)) {
        name = prompt('Enter your name');
    }

    let family = null;
    while (!family || !isNaN(family)) {
        family = prompt('Enter your last name');
    }

    let birthday = null;
    while (!birthday || isNaN(birthday)) {
        birthday = prompt('Enter your birthday (like: "dd.mm.YYYY", example: 19.03.1991)');
        if (birthday) {
            const dateArr = birthday.split('.');
            if (dateArr.length !== 3) {
                birthday = null;
                continue;
            }

            for (let i in dateArr) {
                dateArr[i] = parseInt(dateArr[i]);
                if (!dateArr[i] || isNaN(dateArr[i])) {
                    birthday = null;
                    break;
                }
            }
            if (!birthday) {
                continue;
            }
            dateArr[1] -= 1;

            birthday = new Date(dateArr[2], dateArr[1], dateArr[0]);
        }
    }

    return {
        _firstName: name,
        _lastName: family,
        _birthday: birthday,
        getLogin: function () {
            return this._firstName.charAt(0).toLowerCase() + this._lastName.toLowerCase();
        },
        set firstName(value) {},
        set lastName(val) {},
        setFirstName: function (val) {
            this._firstName = val;
        },
        getFirstName: function () {
            return this._firstName;
        },
        setLastName: function (val) {
            this._lastName = val;
        },
        getLastName: function () {
            return this._lastName;
        },
        getAge() {
            const now = Date.now();
            let diff = (now - this._birthday.getTime()) / 1000;
            diff /= (60 * 60 * 24);
            return Math.abs(Math.round(diff/365.25));
        },
        getPassword() {
            return this._firstName.charAt(0).toUpperCase() + this._lastName.toLowerCase() + this._birthday.getFullYear();
        }
    };
}

const user = createNewUser();

console.log(user.getLogin());
console.log(user.getPassword());
console.log(user.getAge());

user.setFirstName('Gheorghii');
user.setLastName('Iabanji');

user.lastName = 'Geo';
user.firstName = "Iab";

console.log(user);


