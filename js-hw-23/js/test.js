let inputPrice = document.createElement('input');
inputPrice.setAttribute('type', 'number');
inputPrice.setAttribute('id', 'price');
inputPrice.setAttribute('placeholder', 'Price');

document.body.appendChild(inputPrice);

inputPrice.addEventListener('focus', function (e) {
    this.style.outline = 'none';
    this.style.border = '2px solid green';
});

inputPrice.addEventListener('focusout', function (e) {
    if (Number(inputPrice.value) >= 0) {
        if (document.getElementById('down-span')) {
            document.getElementById('down-span').innerText = '';
        }
        let span = document.getElementById('up-span');
        if (!span) {
            span = document.createElement('span');
            span.setAttribute('id', 'up-span');
            this.before(span);
            this.before(document.createElement('br'));
        }
        span.innerText = 'Поточна ціна: ' + this.value + ' ';
        let xBut = document.createElement('button');
        xBut.setAttribute('id', 'x');
        xBut.innerText = 'X';
        span.appendChild(xBut);
        span.style.color = 'green';
        xBut.addEventListener('click', function (e) {
            span.innerHTML = '';
            inputPrice.value = '';
        });
    } else {
        if (document.getElementById('up-span')) {
            document.getElementById('up-span').innerText = '';
        }
        this.style.border = '2px solid red';
        let downSpan = document.getElementById('down-span');
        if (!downSpan) {
            downSpan = document.createElement('span');
            downSpan.setAttribute('id', 'down-span');
            this.after(downSpan);
            this.after(document.createElement('br'));
        }
        downSpan.innerText = 'Please enter correct price';
    }
});