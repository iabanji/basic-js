const createList = (arr, mainElement) => {
    if (! arr instanceof Array || !mainElement || !arr.length) {
        return false;
    }

    let ul = document.createElement('ul');

    for (let i in arr) {
        if (arr[i] instanceof Array || typeof arr[i] === 'string') {
            let li = document.createElement('li');
            if (arr[i] instanceof Array) {
                createList(arr[i], li);
            } else {
                li.textContent = arr[i];
            }
            ul.appendChild(li);
        }
    }

    mainElement.appendChild(ul);
}

createList(["hello", "world", [["abjfgjjjg", "yyyyy"], "test"], "Kiev", "Kharkiv", "Odessa", "Lviv", 'Nikolaev'], document.body);

let secs = 3;

let timer = setInterval(tick,1000);
console.log('Осталось '+(secs)+' секунд');
function tick(){
    console.log('Осталось '+(--secs)+' секунд');
}
setTimeout(() => {
    document.body.innerText = '';
    clearInterval(timer);
}, 3000);


