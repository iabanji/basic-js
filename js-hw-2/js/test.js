const calcResult = (a, b, symbol) => {
    switch (symbol) {
        case '+':
            return a + b;
        case '-':
            return a - b;
        case '/':
            return a / b;
        case '*':
            return a * b;
        default:
            return false;
    }
}

let num1 = false;

while (!num1 || isNaN(num1)) {
    num1 = +prompt('Enter a number:', '5');
}

let num2 = false;

while (!num2 || isNaN(num2)) {
    num2 = +prompt('Enter a number:', '5');
}

let znak = false;

while (!znak || (znak !== '+' && znak !== '-' && znak !== '/' && znak !== '*')) {
    znak = prompt('Enter a symbol (/*+-):', '+');
}

const result = calcResult(num1, num2, znak);
console.log(result);

