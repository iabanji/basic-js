let table = document.createElement('table');

for (let i = 0; i < 30; i++) {
    let tr = document.createElement('tr');

    for (let j = 0; j < 30; j++) {
        let td = document.createElement('td');

        for (let k = 0; k < 5; k++) {
            for (let m = 0; m < 5; m++) {
                if (k === 0 || k === 4 || m === 0 || m === 4) {
                    td.innerHTML += '`';
                } else {
                    td.innerHTML += '&nbsp;';
                }
            }
            if (k !== 4) {
                td.appendChild(document.createElement('br'));
            }
        }

        tr.appendChild(td);
    }

    table.appendChild(tr);
}

document.body.appendChild(table);

table.addEventListener('click', function (e) {
    if (e.target.nodeName === 'TD') {
        if (e.target.style.backgroundColor === 'rgb(0, 0, 0)') {
            e.target.style.backgroundColor = '#ffffff';
        } else if(e.target.style.backgroundColor === 'rgb(255, 255, 255)') {
            e.target.style.backgroundColor = '#000000';
        } else {
            e.target.style.backgroundColor = '#000000';
        }
    }
});
