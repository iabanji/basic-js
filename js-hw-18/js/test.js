const cloneObject = (obj) => {
    if (typeof obj !== 'object') {
        return false;
    }

    let cloneObj = {};

    for (let key in obj) {
        if (typeof obj[key] !== 'object') {
            cloneObj[key] = obj[key];
        } else {
            cloneObj[key] = cloneObject(obj[key]);
        }
    }

    return cloneObj;
}

const a = {
    'key1' : '123',
    'arr': [1,2,3, [3,4,5,6]],
    'obj': {
        'key2': 'test'
    }
};

console.log(cloneObject(a))
