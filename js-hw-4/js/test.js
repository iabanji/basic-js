const calcResult = (a, b, sym) => {
    switch (sym) {
        case '+':
            return a + b;
        case '-':
            return a - b;
        case '/':
            return a / b;
        case '*':
            return a * b;
    }
}

let a = false;
while (!a) {
    a = +prompt('Enter first number', '5');
}

let b = false;
while (!b) {
    b = +prompt('Enter second number', '5');
}

let sym = false;
while (!sym || (sym !== '+' && sym !== '-' && sym !== '/' && sym !== '*')) {
    sym = prompt('Enter symbol (+ - / *)');
}

console.log(calcResult(a,b,sym));




