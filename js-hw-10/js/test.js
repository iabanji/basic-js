const defaultActiveElement = document.querySelector('li.tabs-title.active');

document.querySelectorAll('.tabs-title').forEach((el) => {
    if (el.id !== defaultActiveElement.id) {
        document.querySelectorAll('.tabs-content-li').forEach((li) => {
            if (li.id !== defaultActiveElement.id + '-text') {
                li.style.display = 'none';
            }
        });
    }
    el.addEventListener('click', (e) => {
        const activeElement = document.querySelector('.active');
        document.getElementById(activeElement.id + '-text').style.display = 'none';
        activeElement.classList.remove('active');

        e.target.classList.add('active');
        document.getElementById(e.target.id + '-text').style.display = 'block';
    });
});