const arraySum = (arr) => {
    let sum = 0;
    for (let i in arr) {
        sum += arr[i];
    }
    return sum;
}

const calcDateEnd = (devPoints, taskPoints, dateEnd) => {
    const devPointsSum = arraySum(devPoints);
    const taskPointsSum = arraySum(taskPoints);
    let devDaysForEndJob = taskPointsSum / devPointsSum;

    const oneDayMilliseconds = 24 * 60 * 60 * 1000;
    const timeEnd = dateEnd.getTime();
    let nowTimestamp = Date.now().valueOf();
    let copyDevDays = devDaysForEndJob;
    while (copyDevDays > 1) {
        let nowDate = new Date(nowTimestamp);
        switch (nowDate.getDay()) {
            case 6:
                nowTimestamp += oneDayMilliseconds;
                break;
            case 0:
                nowTimestamp += oneDayMilliseconds;
                break;
            default:
                nowTimestamp += oneDayMilliseconds;
                copyDevDays--;
                break;
        }
    }

    nowTimestamp += copyDevDays * 3600 * 1000;
    let nowDate = new Date(nowTimestamp);
    switch (nowDate.getDay()) {
        case 6:
            nowTimestamp += (2 * oneDayMilliseconds);
            break;
        case 0:
            nowTimestamp += oneDayMilliseconds;
            break;
    }

    if (nowTimestamp > timeEnd) {
        let countHours = 0;
        let copyTimeEnd = timeEnd;
        while (copyTimeEnd < nowTimestamp) {
            let nowDay = new Date(copyTimeEnd);
            switch (nowDay.getDay()) {
                case 6:
                    copyTimeEnd += oneDayMilliseconds;
                    break;
                case 0:
                    copyTimeEnd += oneDayMilliseconds;
                    break;
                default:
                    copyTimeEnd += oneDayMilliseconds;
                    countHours += 8;
                    break;
            }
        }
        alert('Команді розробників доведеться витратити додатково '+ countHours +' годин після дедлайну, щоб виконати всі завдання в беклозі');
    } else {
        let countDais = 0;
        while (timeEnd > nowTimestamp) {
            let nowDay = new Date(nowTimestamp);
            switch (nowDay.getDay()) {
                case 6:
                    nowTimestamp += oneDayMilliseconds;
                    break;
                case 0:
                    nowTimestamp += oneDayMilliseconds;
                    break;
                default:
                    nowTimestamp += oneDayMilliseconds;
                    countDais++;
                    break;
            }
        }
        alert('Усі завдання будуть успішно виконані за '+ countDais +' днів до настання дедлайну!');
    }
}

calcDateEnd([2,2,2,2,2, 20, 20], [100,100,100], new Date(2023, 6, 6))
