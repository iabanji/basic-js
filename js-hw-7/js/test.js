const filterBy = (arr, type) => {
    if (!arr instanceof Array) {
        return false;
    }
    return arr.filter(val => typeof val !== type);
}

console.log(filterBy([12, '22', true, {a:1}, 85.2, 'abc', null], 'string'));
console.log(filterBy([12, '22', true, {a:1}, 85.2, 'abc', null], 'boolean'));
console.log(filterBy([12, '22', true, {a:1}, 85.2, 'abc', null], 'object'));
console.log(filterBy([12, '22', true, {a:1}, 85.2, 'abc', null], 'number'));


