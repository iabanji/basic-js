const student = {};

let name = null;
while (!name) {
    name = prompt('Input your first name:');
}

let lastName = null;

while (!lastName) {
    lastName = prompt('Input your last name:');
}

student.name = name;
student.last_name = lastName;
student.marks = {};

let flag = true;

while (flag) {
    let subjectName = '';
    while ((typeof subjectName === 'string') && !subjectName.length) {
        subjectName = prompt('Input your subject name:');
    }
    if (!subjectName) {
        flag = false;
        break;
    }

    let mark = null;
    while (typeof mark !== 'number') {
        mark = prompt('Input your mark:');
        if (mark === null) {
            mark = 0;
            flag = false;
            break;
        }
        mark = Number(mark);
    }
    student.marks[subjectName] = mark;
}

let countNegativeMarks = 0;
let sum = 0;
let counter = 0;

for (let key in student.marks) {
    counter++;
    sum += student.marks[key];
    if (student.marks[key] < 4) {
        countNegativeMarks++;
    }
}
if (!countNegativeMarks && counter > 0) {
    alert('Студент переведено на наступний курс');
}

let mid = 0;
if (counter > 0) {
    mid = sum / counter;
}

if(mid > 7) {
    alert('Студенту призначено стипендію');
}


console.log(student)

console.log('average score: ' + mid);
