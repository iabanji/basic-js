//1
document.querySelectorAll('p').forEach((item) => {
    item.style.backgroundColor = '#ff0000';
});

//2
const listId = document.getElementById('optionsList');
console.log(listId);
console.log(listId.parentElement);
listId.childNodes.forEach((el) => {
    console.log(el.nodeName + " " + el.nodeType);
    console.log(el);
});
console.log(listId.childNodes);

//3
document.querySelector('#testParagraph').textContent = 'This is a paragraph';

//4 - 5
document.querySelector('.main-header').childNodes.forEach((element) => {
    if (element.nodeName !== "#text") {
        element.setAttribute('class', 'nav-item');
    }
});

//6
document.querySelectorAll('.section-title').forEach((elem) => {
    elem.classList.remove('section-title');
});


