const pass = document.getElementById('pass');
const confirmPass = document.getElementById('confirm-pass');
const passIcon = document.getElementById('pass-eye-icon');
const confirmPassIcon = document.getElementById('confirm-pass-eye-icon');

const toggleIcon = (el) => {
    if (el.classList.contains('fa-eye')) {
        el.classList.remove('fa-eye');
        el.classList.add('fa-eye-slash');
    } else {
        el.classList.remove('fa-eye-slash');
        el.classList.add('fa-eye');
    }
}

const toggleInputType = (el) => {
    if (el.type === 'password') {
        el.type = 'text';
    } else {
        el.type = 'password';
    }
}

passIcon.addEventListener('click', (icon) => {
    toggleIcon(icon.target);
    toggleInputType(document.getElementById(icon.target.getAttribute('data-input-id')));
});

confirmPassIcon.addEventListener('click', (icon) => {
    toggleIcon(icon.target);
    toggleInputType(document.getElementById(icon.target.getAttribute('data-input-id')));
});

document.querySelector('form').addEventListener("submit", (event) => {
    if (pass.value === confirmPass.value) {
        document.getElementById('error-text').innerText = '';
        alert('You are welcome');
    } else {
        document.getElementById('error-text').innerText = 'Потрібно ввести однакові значення';
    }
    event.preventDefault();
});