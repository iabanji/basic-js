const createNewUser = () => {
    let name = null;
    while (!name || !isNaN(name)) {
        name = prompt('Enter your name');
    }

    let family = null;
    while (!family || !isNaN(family)) {
        family = prompt('Enter your last name');
    }

    return {
        _firstName: name,
        _lastName: family,
        getLogin: function () {
            return this._firstName.charAt(0).toLowerCase() + this._lastName.toLowerCase();
        },
        set firstName(value) {},
        set lastName(val) {},
        setFirstName: function (val) {
            this._firstName = val;
        },
        getFirstName: function () {
            return this._firstName;
        },
        setLastName: function (val) {
            this._lastName = val;
        },
        getLastName: function () {
            return this._lastName;
        }
    };
}

const user = createNewUser();

console.log(user.getLogin());

user.setFirstName('Gheorghii');
user.setLastName('Iabanji');

user.lastName = 'Geo';
user.firstName = "Iab";

console.log(user);


