const imagesCount = document.querySelectorAll('.image-to-show').length;
document.querySelectorAll('.image-to-show:not(:first-child)').forEach((el) => {
    el.style.display = 'none';
});

let imageNumber = 1;
let fadeIn = null;
let fadeOut = null;
let time = null;
let seconds = 3000;
let opacity2 = 0;
let opacity = 1;

function fadeInFunc(img) {
    fadeIn = setInterval(() => {
        img.style.opacity = opacity;
        if (opacity <= 0.02) {
            img.style.display = 'none';
            clearInterval(fadeIn);
            opacity = 1;


            seconds = 3000;
            document.getElementById('timer').innerText = 'Timer 3:000';
            imageNumber === imagesCount ? imageNumber = 1 : ++imageNumber;
            let img2 = document.querySelector(`.image-to-show:nth-child(${imageNumber})`);
            img2.style.opacity = opacity2;
            img2.style.display = '';
            fadeOutFunc(img2);
        } else {
            opacity -= 0.01;
        }
    }, 5);
}

function fadeOutFunc(img2) {
    fadeOut = setInterval(() => {
        img2.style.opacity = opacity2;
        if (opacity2 >= 0.98) {
            clearInterval(fadeOut);
            opacity2 = 0;
        } else {
            opacity2 += 0.01;
        }
    }, 5);
}

function timer () {
    let interval = 50;
    if (seconds === 3000) {
        document.getElementById('timer').innerText = 'Timer 3:000';
    }
    time = setInterval(() => {
        seconds -= interval;
        document.getElementById('timer').innerText = 'Timer ' + Math.floor(seconds / 1000) + ':' + Math.round(((seconds / 1000) % 1) * 1000);
        if (seconds === 500) {
            let img = document.querySelector(`.image-to-show:nth-child(${imageNumber})`);
            fadeInFunc(img);
        }
        if (seconds <= 0) {
            seconds = 3000;
        }
    }, interval);
}

timer();
document.getElementById('stop').addEventListener('click', (e) => {
    if (fadeIn) {
        clearInterval(fadeIn);
        fadeIn = null;
    }
    if (fadeOut) {
        clearInterval(fadeOut);
        fadeOut = null;
    }
    if (time) {
        clearInterval(time);
        time = null;
    }
});

document.getElementById('continue').addEventListener('click', (e) => {
    if (!time) {
        timer();
        let icon = document.querySelector(`.image-to-show:nth-child(${imageNumber})`);
        if (opacity != 1) {
            fadeInFunc(icon);
        }
        if (opacity2 != 0) {
            fadeOutFunc(icon);
        }
    }
});